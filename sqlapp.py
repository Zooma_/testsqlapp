# -*- coding: utf-8 -*-
from loguru import logger
from random import choice, random
import argparse
import csv
import sqlite3
import time


class SqlApp:
    try:
        conn = sqlite3.connect('sqlapp.db')
        cursor = conn.cursor()
        logger.success('Database success connected.')
    except sqlite3.Error as error:
        print('Error while working with SQLite: ', error)
    finally:
        if not conn:
            conn.close()
            print('sqlite connection is closed')

    def __init__(self):
        parser = argparse.ArgumentParser(argument_default='-h')
        subparsers = parser.add_subparsers(description='Execute some commands for SQL manipulations',
                                           help='Available commands:')

        command1_parser = subparsers.add_parser('1', help='Create db and table')
        command1_parser.set_defaults(func=self.create_table)

        command2_parser = subparsers.add_parser('2', help='Add row to db')
        command2_parser.add_argument('fio', nargs=3)
        command2_parser.add_argument('date_birth')
        command2_parser.add_argument('gender')
        command2_parser.set_defaults(func=self.add_person)

        command3_parser = subparsers.add_parser('3', help='Show table ordered by fio')
        command3_parser.set_defaults(func=self.show_table)

        command4_parser = subparsers.add_parser('4', help='Fill table by 1000000 rows')
        command4_parser.set_defaults(func=self.add_one_million_rows)

        command5_parser = subparsers.add_parser('5', help='Measure the execution time my query')
        command5_parser.set_defaults(func=self.my_query)

        self.args = parser.parse_args()

    def my_query(self, *args):
        try:
            t_start = time.process_time()
            self.cursor.execute("SELECT * FROM peoples WHERE gender='male' AND fio LIKE 'F%'; ")
            t_end = time.process_time()
            logger.success('Query success executed.')
            for row in self.cursor.fetchall():
                for column in row:
                    print(column, end=' ')
                print('')
            print(t_end - t_start, ' seconds')

        except sqlite3.Error as error:
            logger.error('Error while working with SQLite: {}', error)

    def create_table(self):
        try:
            self.cursor.execute("""CREATE TABLE peoples 
                                (id INTEGER PRIMARY KEY,
                                 fio TEXT,
                                 date_birth TEXT,
                                 gender TEXT);""")
            logger.success('Table success created.')
        except sqlite3.Error as error:
            logger.error('Error while working with SQLite: {}', error)

    def add_person(self, *args):
        person = (' '.join(args[0].fio), args[0].date_birth, args[0].gender)
        print(type(person))
        print(person)
        try:
            self.cursor.executemany("INSERT INTO peoples(fio, date_birth, gender) VALUES (?, ?, ?)", (person, ))
            logger.success('Row success inserted with values: {}.', person)
        except sqlite3.Error as error:
            logger.error('Error while working with SQLite: {}', error)

            self.conn.commit()

    def show_table(self, args):
        self.cursor.execute("""SELECT DISTINCT 
                                fio, 
                                date_birth, 
                                gender, 
                                strftime('%Y', 'now') - strftime('%Y', date_birth) 
                                FROM peoples ORDER BY fio""")
        for row in self.cursor.fetchall():
            for column in row:
                print(column, end=' ')
            print('')

    @staticmethod
    def generate_person():
        female_first_names = []
        female_middle_names = []
        female_last_names = []

        male_first_names = []
        male_middle_names = []
        male_last_names = []

        gender = choice(('male', 'female'))

        with open('female_names.txt') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            for row in csv_reader:
                female_first_names.append(row[0])
                female_middle_names.append(row[1])
                female_last_names.append(row[2])

        with open('male_names.txt') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            for row in csv_reader:
                male_first_names.append(row[0])
                male_middle_names.append(row[1])
                male_last_names.append(row[2])

        if gender == 'female':
            fio = ' '.join((choice(female_first_names), choice(female_middle_names), choice(female_last_names)))
            return {
                'fio': fio,
                'gender': gender
            }
        elif gender == 'male':
            fio = ' '.join((choice(male_first_names), choice(male_middle_names), choice(male_last_names)))
            return {
                'fio': fio,
                'gender': gender
            }
        else:
            print('Unknown gender')

    @staticmethod
    def str_time_prop(start, end, fmt, prop):
        """Get a time at a proportion of a range of two formatted times.

        start and end should be strings specifying times formated in the
        given format (strftime-style), giving an interval [start, end].
        prop specifies how a proportion of the interval to be taken after
        start.  The returned time will be in the specified format.
        """

        stime = time.mktime(time.strptime(start, fmt))
        etime = time.mktime(time.strptime(end, fmt))

        ptime = stime + prop * (etime - stime)

        return time.strftime(fmt, time.localtime(ptime))

    @staticmethod
    def random_date(start, end, prop):
        return SqlApp.str_time_prop(start, end, '%Y-%m-%d', prop)

    def add_one_million_rows(self, args):
        for row in range(0, 1000000):
            human = self.generate_person()
            generated_date = self.random_date("1990-1-1", "2005-1-1", random())
            person = (human['fio'],
                      generated_date,
                      human['gender'])
            try:
                self.cursor.executemany("insert into peoples(fio, date_birth, gender) values (?, ?, ?)", (person,))
                logger.success('Row success inserted with values: {}.', person)
            except sqlite3.OperationalError:
                self.create_table()
            except sqlite3.Error as error:
                logger.error('Error while working with SQLite: {}', error)

            self.conn.commit()

    def run(self):
        self.args.func(self.args)


if __name__ == '__main__':
    app = SqlApp()
    app.run()
